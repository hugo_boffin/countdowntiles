﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Navigation;
using System.Windows.Threading;
using CountdownLib;
using CountdownLib.ViewModels;
using CountdownTiles.Resources;
using Microsoft.Phone.Shell;

namespace CountdownTiles
{
    public partial class DetailPage
    {
        private readonly DispatcherTimer dt = new DispatcherTimer();
        private bool _newCountdown;
        private CountDownViewModel backup;

        public DetailPage()
        {
            InitializeComponent();
            BuildLocalizedApplicationBar();
        }

        // Legt beim Navigieren auf der Seite den Datenkontext auf das in der Liste ausgewählte Element fest
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (App.ViewModel == null)
            {
                App.ViewModel = Global.PlatformAbstraction.Load();
            }

            if (DataContext == null)
            {
                string selectedIndex = "";

                string pin;
                if (NavigationContext.QueryString.TryGetValue("newCountdown", out pin))
                {
                    _newCountdown = bool.Parse(pin);
                }
                else
                {
                    _newCountdown = false;
                }

                if (NavigationContext.QueryString.TryGetValue("selectedItem", out selectedIndex))
                {
                    Guid guid = Guid.Parse(selectedIndex);
                    DataContext = backup = App.ViewModel.CountDownViewModels.FirstOrDefault(count => count.Guid == guid);
                }
            }

            dt.Interval = new TimeSpan(0, 0, 0, 1, 0);
            dt.Tick += (sender, args) =>
                           {
                               var countDownViewModel = DataContext as CountDownViewModel;
                               if (countDownViewModel != null)
                                   countDownViewModel.UpdateTimerBindings();
                           };
            dt.Start();

            MainPage.InitializeBackgroundAgent();
        }


        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            dt.Stop();
            base.OnNavigatedFrom(e);
        }

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            App.ViewModel.RemoveCountdown(DataContext as CountDownViewModel);
            if (!_newCountdown) // Neues Countdownobject - da brauchts kein adden
            {
                App.ViewModel.AddCountdown(backup);
            }


            base.OnBackKeyPress(e);
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();

            var saveIcon = new ApplicationBarIconButton(new Uri("/Assets/AppBar/save.png", UriKind.Relative))
                               {
                                   Text = AppResources.AppBarAddText
                               };

            var deleteIcon = new ApplicationBarIconButton(new Uri("/Assets/AppBar/delete.png", UriKind.Relative))
                                 {
                                     Text = AppResources.AppBarTrashText
                                 };

            ApplicationBar.Buttons.Add(saveIcon);
            saveIcon.Click += SaveIconOnClick;

            ApplicationBar.Buttons.Add(deleteIcon);
            deleteIcon.Click += DeleteIconOnClick;
        }

        private void DeleteIconOnClick(object sender, EventArgs eventArgs)
        {
            var cdvm = (DataContext as CountDownViewModel);

            if (cdvm != null)
            {
                cdvm.Pinned = false;
                App.ViewModel.RemoveCountdown(cdvm);
            }

            if (!_newCountdown) // Neues Countdownobject - da brauchts kein speichern
            {
                Global.PlatformAbstraction.Save(App.ViewModel);
            }

            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
            else
            {
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                NavigationService.RemoveBackEntry();
            }
        }

        private void SaveIconOnClick(object sender, EventArgs eventArgs)
        {
            var cdvm = (DataContext as CountDownViewModel);

            if (cdvm != null)
            {
                // Application bar does not take focus, but textbox text changes are no update until focus-lost, so
                // we update them manually
                object focusedElement = FocusManager.GetFocusedElement();
                if (focusedElement != null && focusedElement is TextBox)
                {
                    BindingExpression binding = (focusedElement as TextBox).GetBindingExpression(TextBox.TextProperty);
                    if (binding != null)
                        binding.UpdateSource();
                }

                Global.PlatformAbstraction.Save(App.ViewModel);
            }

            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
            else
            {
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                NavigationService.RemoveBackEntry();
            }

            if (cdvm != null)
            {
                if (_newCountdown)
                {
                    cdvm.Pinned = true;
                }
                else
                {
                    Global.PlatformAbstraction.UpdateTile(cdvm);
                }
            }
            _newCountdown = false;
        }
    }
}