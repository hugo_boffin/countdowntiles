﻿using System;
using System.Threading;
using System.Windows;
using CountdownLib.ViewModels;
using Microsoft.Phone.Shell;

namespace CountdownTiles
{
    public class PlatformSpecificsFullWP8 : PlatformSpecificsLightWP8
    {
        protected override void CreateLiveTile(CountDownViewModel countDownViewModel)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                var tileData = CreateTileData(countDownViewModel);
                ShellTile.Create(new Uri("/DetailPage.xaml?selectedItem=" + countDownViewModel.Id, UriKind.Relative),
                    tileData, true);
            });
        }
    }
}