﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using Windows.Phone.System.Power;
using CountdownLib;
using CountdownLib.Models;
using CountdownLib.ViewModels;
using CountdownTiles.Resources;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Scheduler;
using Microsoft.Phone.Shell;
using TileUpdateAgent;

namespace CountdownTiles
{
    public partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
            BuildLocalizedApplicationBar();
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();

            var addIcon = new ApplicationBarIconButton(new Uri("/Assets/AppBar/add.png", UriKind.Relative))
                              {
                                  Text = AppResources.AppBarAddText
                              };
            addIcon.Click += AddIconOnClick;

            ApplicationBar.Buttons.Add(addIcon);

            var about = new ApplicationBarMenuItem(AppResources.AppBarAboutText);
            about.Click += AboutClick;
            ApplicationBar.MenuItems.Add(about);
        }

        private void AboutClick(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }

        private void AddIconOnClick(object sender, EventArgs eventArgs)
        {
            var neu = new CountDownViewModel(new Countdown
                                                 {
                                                     Color =
                                                         ((SolidColorBrush)
                                                          Application.Current.Resources["PhoneAccentBrush"]).Color
                                                 });
            App.ViewModel.AddCountdown(neu);
            NavigationService.Navigate(
                new Uri("/DetailPage.xaml?newCountdown=true&selectedItem=" + neu.Id, UriKind.Relative));
        }

        private void CountdownList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CountdownList.SelectedItem == null)
                return;

            NavigationService.Navigate(
                new Uri("/DetailPage.xaml?selectedItem=" + (CountdownList.SelectedItem as CountDownViewModel).Id,
                        UriKind.Relative));

            CountdownList.SelectedItem = null;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (App.ViewModel == null)
            {
                App.ViewModel = Global.PlatformAbstraction.Load();
            }

            Global.PlatformAbstraction.RefreshPinStates(App.ViewModel);

            CountdownList.ItemsSource = App.ViewModel.CountDownViewModels.OrderBy(x => x.Target);
            
            InitializeBackgroundAgent();

//            var progressIndicator = SystemTray.ProgressIndicator; 
//            if (progressIndicator != null) { return; } 
//            progressIndicator = new ProgressIndicator(); 
//            SystemTray.SetProgressIndicator(this, progressIndicator);
//            progressIndicator.IsIndeterminate = true;
//            progressIndicator.Text = "Fleischwurst";
//            progressIndicator.IsVisible = true;

//            this.IsEnabled = false;
//            this.ApplicationBar.IsVisible = false;
            
//            ApplicationBar.Disable
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;

            if (menuItem == null) return;

            var cdvm = (menuItem.DataContext as CountDownViewModel);

            cdvm.Pinned = false;
            App.ViewModel.RemoveCountdown(cdvm);

            Global.PlatformAbstraction.Save(App.ViewModel);
            CountdownList.ItemsSource = App.ViewModel.CountDownViewModels.OrderBy(x => x.Target);
        }

        public static void InitializeBackgroundAgent()
        {
            bool backgroundAgentDisabled = PowerManager.PowerSavingMode == PowerSavingMode.On;

            var periodicTask = ScheduledActionService.Find(ScheduledAgent.periodicTaskName) as PeriodicTask;

            if (periodicTask != null)
            {
                Debug.WriteLine(periodicTask.LastExitReason);
                Debug.WriteLine(periodicTask.IsScheduled);
                Debug.WriteLine(periodicTask.IsEnabled);

                if (!periodicTask.IsEnabled)
                    backgroundAgentDisabled = true;

                ScheduledActionService.Remove(ScheduledAgent.periodicTaskName);
            }

            try
            {
                periodicTask = new PeriodicTask(ScheduledAgent.periodicTaskName) { Description = "Updates Live Tiles" };
                ScheduledActionService.Add(periodicTask);

                Debug.WriteLine("Registering PeriodicTask OK!");
            }
            catch (InvalidOperationException exception)
            {
                Debug.WriteLine("InvalidOperationException");

                if (exception.Message.Contains("BNS Error: The action is disabled"))
                {
                    backgroundAgentDisabled = true;
                }
                else if (exception.Message.Contains("BNS Error: The maximum number of ScheduledActions of this type have already been added."))
                {
                    // No user action required. The system prompts the user when the hard limit of periodic tasks has been reached.
                    Debug.WriteLine("InvalidOperationException - No user action required. The system prompts the user when the hard limit of periodic tasks has been reached.", "Countdown Tiles", MessageBoxButton.OK);
                }
                else
                {
                    Debug.WriteLine("InvalidOperationException - InvalidOperationException", "Countdown Tiles", MessageBoxButton.OK);
                }
            }
            catch (SchedulerServiceException)
            {
                // No user action required.
                Debug.WriteLine("SchedulerServiceException - No user action required.", "Countdown Tiles", MessageBoxButton.OK);
            }

            if (backgroundAgentDisabled && !App.warnedAboutBackgroundAgents) {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                    MessageBox.Show(AppResources.BackgroundAgentsDisabled, "Countdown Tiles", MessageBoxButton.OK));
                App.warnedAboutBackgroundAgents = true;
            }

#if DEBUG
            ScheduledActionService.LaunchForTest(ScheduledAgent.periodicTaskName, TimeSpan.FromSeconds(30));
#endif
        }
    }
}