﻿using System.Windows.Navigation;
using Coding4Fun.Toolkit.Controls.Common;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;

namespace CountdownTiles
{
    public partial class AboutPage : PhoneApplicationPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Version.Text = PhoneHelper.GetAppAttribute("Version");

            rateAppLink.Click += (sender, args) =>
            {
                MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();
                marketplaceReviewTask.Show();
            };
            base.OnNavigatedTo(e);
        }
    }
}