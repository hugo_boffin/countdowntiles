﻿using System;
using System.Diagnostics;
using System.Windows;
using CountdownLib;
using CountdownTiles;
using Microsoft.Phone.Scheduler;

namespace TileUpdateAgent
{
    public class ScheduledAgent : ScheduledTaskAgent
    {
        /// <remarks>
        ///     ScheduledAgent constructor, initializes the UnhandledException handler
        /// </remarks>
        static ScheduledAgent()
        {
            // Subscribe to the managed exception handler
            Deployment.Current.Dispatcher.BeginInvoke(
                delegate { Application.Current.UnhandledException += UnhandledException; });
        }

        /// Code to execute on Unhandled Exceptions
        private static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                Debugger.Break();
            }
        }

        public const string periodicTaskName = "CountdownTileUpdateTask";

        /// <summary>
        ///     Agent that runs a scheduled task
        /// </summary>
        /// <param name="task">
        ///     The invoked task
        /// </param>
        /// <remarks>
        ///     This method is called when a periodic or resource intensive task is invoked
        /// </remarks>
        protected override void OnInvoke(ScheduledTask task)
        {
            Debug.WriteLine("Render from Background Agent");
            Global.PlatformAbstraction = new PlatformSpecificsLightWP8();
            Global.PlatformAbstraction.UpdateTiles();
            //Deployment.Current.Dispatcher.BeginInvoke(() =>
            //                                              {
            //                                                  var toast = new ShellToast {Title = "Pingu pingu",Content = "123"};
            //                                                  toast.Show();
            //                                              });

#if DEBUG
            ScheduledActionService.LaunchForTest(periodicTaskName, TimeSpan.FromSeconds(30));
#endif
            NotifyComplete();
        }
    }
}