﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Serialization;
using CountdownLib;
using CountdownLib.Models;
using CountdownLib.Resources;
using CountdownLib.ViewModels;
using Microsoft.Phone.Shell;

namespace CountdownTiles
{
    public class PlatformSpecificsLightWP8 : IPlatformAbstraction
    {
        public void Save(MainViewModel mainViewModel)
        {
            var mutex = new Mutex(false, MutexName);

            mutex.WaitOne();
            try
            {
                var xmlWriterSettings = new XmlWriterSettings {Indent = true};

                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(CountDownXml, FileMode.Create))
                    {
                        var serializer = new XmlSerializer(typeof (CountdownRepository));
                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, mainViewModel.Repository);
                        }
                    }
                }
            }
            finally
            {
                mutex.ReleaseMutex();

                UpdatePrimaryTile(mainViewModel);
                //Debugger.Log(0,"123","Save\n");
            }
        }

        public MainViewModel Load()
        {
            var mutex = new Mutex(false, MutexName);

            mutex.WaitOne();
            try
            {
                //Debugger.Log(0, "123", "Load\n");
                if (!IsolatedStorageFile.GetUserStoreForApplication().FileExists(CountDownXml))
                {
                    // TODO: CreateSampleData
                    return new MainViewModel();
                }
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (
                        IsolatedStorageFileStream fileStream = myIsolatedStorage.OpenFile(CountDownXml, FileMode.Open))
                    {
                        var serializer = new XmlSerializer(typeof (CountdownRepository));
                        return new MainViewModel((CountdownRepository) serializer.Deserialize(fileStream));
                    }
                }
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }

        public void SetupTile(CountDownViewModel countDownViewModel)
        {
            ShellTile tile = ShellTile.ActiveTiles.
                                       FirstOrDefault(x => x.NavigationUri.ToString().EndsWith(countDownViewModel.Id));

            if (countDownViewModel.Pinned)
            {
                if (tile == null)
                {
                    CreateLiveTile(countDownViewModel);
                }
            }
            else
            {
                if (tile != null)
                {
                    tile.Delete();
                }
            }
        }

        protected virtual void CreateLiveTile(CountDownViewModel countDownViewModel)
        {
        }

        public void RefreshPinStates(MainViewModel mainViewModel)
        {
            foreach (CountDownViewModel countDown in mainViewModel.CountDownViewModels)
            {
                ShellTile tile =
                    ShellTile.ActiveTiles.FirstOrDefault(x => x.NavigationUri.ToString().EndsWith(countDown.Id));
                countDown.Pinned = tile != null;
            }
        }

        public void UpdateTilesAsync()
        {
            var bw = new BackgroundWorker();
            bw.DoWork += (sender, args) => UpdateTiles();
            bw.RunWorkerAsync();
        }

        public void UpdateTiles()
        {
            Debug.WriteLine("Waiting for Render Mutex");
            var mutex = new Mutex(false, RenderMutexName);

            mutex.WaitOne();
            Debug.WriteLine("Started Rendering");

            MainViewModel mainViewModel = Global.PlatformAbstraction.Load();

            //
            // ---- RENDER ALL TILES
            //

            IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication();

            // Remove orphaned tile images
            string[] files = store.GetFileNames("Shared\\ShellContent\\Tile_*");
            foreach (string fileName in from file in files
                                        let cd =
                                            mainViewModel.CountDownViewModels.FirstOrDefault(x => file.Contains(x.Id))
                                        where cd == null
                                        select file)
            {
                store.DeleteFile("Shared\\ShellContent\\" + fileName);
            }

            // Remove orphaned tiles
            foreach (ShellTile tile in from tile in ShellTile.ActiveTiles
                                       let cd =
                                           mainViewModel.CountDownViewModels.FirstOrDefault(
                                               icd => tile.NavigationUri.ToString().Contains(icd.Id))
                                       where cd == null && tile.NavigationUri.ToString() != "/"
                                       select tile)
            {
                tile.Delete();
            }

            // Update primary tile
            UpdatePrimaryTile(mainViewModel);

            // Update Tile Images, TileData, etc.
            foreach (CountDownViewModel countDown in mainViewModel.CountDownViewModels)
            {
                UpdateTile(countDown);
            }

            Debug.WriteLine("Finished Rendering");
            mutex.ReleaseMutex();
        }

        public void UpdateTile(CountDownViewModel countDownViewModel)
        {
            var mutex = new Mutex(false, SingleRenderMutexName);

            mutex.WaitOne();

            ShellTile tile =
                ShellTile.ActiveTiles.FirstOrDefault(x => x.NavigationUri.ToString().EndsWith(countDownViewModel.Id));

            countDownViewModel.Pinned = tile != null;

            if (countDownViewModel.Pinned)
            {
                // TODO: Mainpage muss blockieren bis fertig angepinned ist (grauer overlay mit warteanzeige, beim start nur pinstates aktualisieren, manuelles aktualisieren in app bar
//                EventWaitHandle wait = new AutoResetEvent(false);
                var x = Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    tile.Update(CreateTileData(countDownViewModel));
//                    wait.Set();
                });
//                wait.WaitOne();
            }

            mutex.ReleaseMutex();
        }
        
        //-----------------------------------------------------

        private const string MutexName = "CountdownConfigMutex";
        private const string CountDownXml = @"Countdowns.xml";
        private const string RenderMutexName = "CountdownRenderMutex";
        private const string SingleRenderMutexName = "CountdownSingleRenderMutex";

        protected ShellTileData CreateTileData(CountDownViewModel countDownViewModel)
        {
            string imageSmallPath = RenderTile(countDownViewModel, "Small", 159, 159);
            string imageMediumPath = RenderTile(countDownViewModel, "Medium", 336, 336);
            string imageWidePath = RenderTile(countDownViewModel, "Wide", 691, 336);

            return new FlipTileData
                       {
#if DEBUG
                           Title = DateTime.Now + ">"+countDownViewModel.Name,
#else
                           Title = countDownViewModel.Name,
#endif
                           //WideBackContent = ,
                           SmallBackgroundImage = new Uri(imageSmallPath),
                           BackgroundImage = new Uri(imageMediumPath),
                           WideBackgroundImage = new Uri(imageWidePath)
                       };
        }

        private string RenderTile(CountDownViewModel cdvm, string type, int w, int h)
        {
            IsolatedStorageFile userStore = IsolatedStorageFile.GetUserStoreForApplication();
            const string shellContentDirectory = "Shared\\ShellContent";
            if (!userStore.DirectoryExists(shellContentDirectory))
                userStore.CreateDirectory(shellContentDirectory);
            string imagePath = shellContentDirectory + "\\Tile_" + type + "_" + cdvm.Guid + (cdvm.SystemColor ? ".png" : ".jpg");
            
            EventWaitHandle wait = new AutoResetEvent(false);

            var frontTile = new Grid { Width = w, Height = h };

            if (!cdvm.SystemColor)
                frontTile.Background = cdvm.ColorBrush;


            var viewbox = new Viewbox
            {
                Stretch = Stretch.Uniform
            };

            var dcblock = new TextBlock
            {
                Foreground = Application.Current.Resources["PhoneForegroundBrush"] as SolidColorBrush,
                FontFamily = new FontFamily("Segoe WP Bold"),
                VerticalAlignment = VerticalAlignment.Center,
                TextAlignment = TextAlignment.Center,
                FontSize = 10,
                Padding = new Thickness(3)
            };

            viewbox.Child = dcblock;

            dcblock.Text = cdvm.DaysToComeTile;

            frontTile.Children.Add(viewbox);
            frontTile.UpdateLayout();
            frontTile.Measure(new Size(w, h));
            frontTile.UpdateLayout();
            frontTile.Arrange(new Rect(0, 0, w, h));
            frontTile.UpdateLayout();

            var bmp = new WriteableBitmap(w, h);
            bmp.Render(frontTile, null);
            bmp.Invalidate();

            frontTile = null;

            Debug.WriteLine("Rendering " + imagePath);
            using (IsolatedStorageFileStream stream = userStore.OpenFile(imagePath, FileMode.Create, FileAccess.Write))
            {
                if (cdvm.SystemColor)
                    bmp.WritePNG(stream);
                else
                    bmp.SaveJpeg(stream, w, h, 0, 100);
                GC.Collect(2, GCCollectionMode.Forced, true); //INFO: Compensates out of memory exception
            }
            GC.Collect(2, GCCollectionMode.Forced, true); //INFO: Compensates out of memory exception

            return "isostore:/" + imagePath.Replace('\\', '/');
        }

        private void UpdatePrimaryTile(MainViewModel mvm)
        {
            foreach (ShellTile tile in ShellTile.ActiveTiles)
            {
                if (tile.NavigationUri.ToString() == "/")
                {
                    var data = new FlipTileData();
                    var cd = mvm.CountDownViewModels.Where(model => ((model.Target - DateTime.Now).TotalSeconds > 0.0)).OrderBy(x => x.Target).FirstOrDefault();
                    if (cd == null)
                    {
                        data.BackContent = string.Empty;
                        data.BackTitle = string.Empty;
                    }
                    else
                    {
                        data.BackContent = LibResources.NextEnds + cd.Target.ToLongDateString();
                        data.BackTitle = cd.Name;

                    }
                    tile.Update(data);
                    return;
                }
            }
        }

        //main.Countdowns.Add(new CountDownViewModel(new DateTime(2013, 6, 10), "Project 1 Deadline", Colors.DarkGray, true, "166a43f1-9450-49f0-8b5e-f19f48e2568d"));
        //main.Countdowns.Add(new CountDownViewModel(new DateTime(2013, 6, 17), "Project 2 Deadline", Colors.Magenta, false, "21ab21cc-cfda-4ce0-97c3-7f9567598080"));
        //main.Countdowns.Add(new CountDownViewModel(new DateTime(2013, 5, 8), "Megs Birthday", Colors.Red, false, "31ab21cc-cfda-4ce0-97c3-7f9567598080"));
        //main.Countdowns.Add(new CountDownViewModel(new DateTime(2013, 5, 2, 23, 59, 59), "App Revolution", Colors.Purple, true, "e3788fc7-5e0e-49dc-ad49-b2d682a61212"));
        //main.Countdowns.Add(new CountDownViewModel(new DateTime(2013, 8, 23), "Marriage", Colors.Brown, false, "b654b8ac-422c-4c99-b2ca-13ad49d294ac"));
        //main.Countdowns.Add(new CountDownViewModel(new DateTime(2013, 8, 3), "Wedding-Eve party", Colors.Orange, true, "666a43f1-9450-49f0-8b5e-f19f48e2568d"));
        //main.Countdowns.Add(new CountDownViewModel(new DateTime(2013, 11, 11, 11, 11, 11), "11.11 11:11", Colors.Green, true, "d67a20ef-fe07-4eca-9c84-8d94933062fd"));
    }
}