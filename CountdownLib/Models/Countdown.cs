﻿using System;
using System.Windows.Media;
using System.Xml.Serialization;
using CountdownLib.Resources;

namespace CountdownLib.Models
{
    [XmlRoot("Countdown")]
    public class Countdown
    {
        public Countdown()
        {
            Color = Colors.Red;
            Name = LibResources.NewCountdown;
            Target = DateTime.Today.AddDays(1);
            Pinned = false;
            Guid = Guid.NewGuid();
            SystemColor = true;
        }

        public Countdown(Countdown clone)
        {
            Pinned = clone.Pinned;
            Color = clone.Color;
            Name = clone.Name;
            Target = clone.Target;
            Guid = clone.Guid;
            SystemColor = clone.SystemColor;
        }

        public Countdown(DateTime target, string name, Color color, bool pinned, bool systemColor, string guid)
        {
            Color = color;
            Name = name;
            Target = target;
            Pinned = pinned;
            SystemColor = systemColor;
            Guid = new Guid(guid);
        }

        [XmlAttribute]
        public bool Pinned { get; set; }

        [XmlAttribute]
        public bool SystemColor { get; set; }

        //[XmlAttribute]
        public Color Color { get; set; }

        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public DateTime Target { get; set; }

        [XmlAttribute]
        public Guid Guid { get; set; }

        //TODO: Änderungen in ViewModel broadcasten?
    }
}