﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CountdownLib.Models
{
    [XmlRoot("Main")]
    public class CountdownRepository
    {
        public CountdownRepository()
        {
            Countdowns = new List<Countdown>();
        }

        [XmlArrayItem(ElementName = "Countdown")]
        public List<Countdown> Countdowns { get; set; }
    }
}