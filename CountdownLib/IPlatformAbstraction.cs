using CountdownLib.ViewModels;

namespace CountdownLib
{
    public interface IPlatformAbstraction
    {
        MainViewModel Load();
        void Save(MainViewModel mainViewModel);
        void RefreshPinStates(MainViewModel mainViewModel);

        void SetupTile(CountDownViewModel countDownViewModel);
        void UpdateTile(CountDownViewModel countDownViewModel);

        void UpdateTilesAsync();
        void UpdateTiles();
    }

    public class Global
    {
        public static IPlatformAbstraction PlatformAbstraction;
    }
}