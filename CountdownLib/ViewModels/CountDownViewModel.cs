using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using CountdownLib.Models;
using CountdownLib.Resources;
using System.Windows;

namespace CountdownLib.ViewModels
{
    public class CountDownViewModel : IComparable<CountDownViewModel>, INotifyPropertyChanged
    {
        #region Variables

        private readonly Countdown _countdown;

        #endregion

        #region Properties

        public DateTime Target
        {
            get { return _countdown.Target; }
            set
            {
                if (value.Equals(_countdown.Target)) return;
                _countdown.Target = value;
                OnPropertyChanged();
            }
        }

        public String Name
        {
            get { return _countdown.Name; }
            set
            {
                if (value == _countdown.Name) return;
                _countdown.Name = value;
                OnPropertyChanged();
            }
        }

        public Color Color
        {
            get { return _countdown.Color; }
            set
            {
                if (value.Equals(_countdown.Color)) return;
                _countdown.Color = value;
                OnPropertyChanged();
            }
        }

        public Boolean SystemColor
        {
            get { return _countdown.SystemColor; }
            set
            {
                if (value.Equals(_countdown.SystemColor)) return;
                _countdown.SystemColor = value;

                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("UseCustomColor"));

                OnPropertyChanged();
            }
        }

        public Visibility UseCustomColor
        {
            get { return _countdown.SystemColor ? Visibility.Collapsed : Visibility.Visible; }
        }   

        public Boolean Pinned
        {
            get { return _countdown.Pinned; }
            set
            {
                if (value.Equals(_countdown.Pinned)) return;

                _countdown.Pinned = value;
                OnPropertyChanged();

                var bw = new BackgroundWorker();
                bw.DoWork += (sender, args) => Global.PlatformAbstraction.SetupTile(this);
                bw.RunWorkerAsync();
            }
        }



        public Guid Guid
        {
            get { return _countdown.Guid; }
        }

        public String Id
        {
            get { return _countdown.Guid.ToString(); }
        }

        public String DaysToComeMsg
        {
            get
            {
                string time;

                TimeSpan span = (Target - DateTime.Now);

                if (span.TotalSeconds < 0.0)
                    time = LibResources.FaelligSeit + -DaysToCome +
                           (DaysToCome == 1 ? LibResources._Tag : LibResources._Tagen);
                else
                    time = LibResources.Noch_ + DaysToCome + (DaysToCome == 1 ? LibResources._Tag : LibResources._Tage);

                return time;
            }
        }

        public String DaysToComeTile
        {
            get
            {
                TimeSpan span = (Target - DateTime.Now);
                int d = DaysToCome;
                if (span.TotalSeconds < 0.0)
                    return "+" + Math.Abs(d);
                else
                    return d.ToString();
            }
        }

        public String PreciseSpanMsg
        {
            get
            {
                TimeSpan span = (Target - DateTime.Now);
                return Math.Abs(span.Hours) +
                       (Math.Abs(span.Hours) == 1 ? LibResources._Stunde_ : LibResources._Stunden_) +
                       Math.Abs(span.Minutes) +
                       (Math.Abs(span.Minutes) == 1 ? LibResources.MinuteAnd : LibResources.MinutesAnd) +
                       Math.Abs(span.Seconds) +
                       (Math.Abs(span.Seconds) == 1 ? LibResources.Second : LibResources.Seconds);
            }
        }
        
        public Brush ColorBrush
        {
            get { return _countdown.SystemColor ? (SolidColorBrush)Application.Current.Resources["PhoneAccentBrush"] : new SolidColorBrush(Color); }
        }

        public string TargetInfo
        {
            get
            {
                string time;

                if (DaysToCome == 0)
                    time = "(" + LibResources.Today + ")";
                else if (DaysToCome < 0)
                    time = " (" + LibResources.PastPreTargetInfo + -DaysToCome +
                           (DaysToCome == 1
                                ? LibResources.PastPostSingularTargetInfo
                                : LibResources.PastPostPluralTargetInfo) + ")";
                else
                    time = " (" + LibResources.PreTargetInfo + DaysToCome +
                           (DaysToCome == 1 ? LibResources.PostSingularTargetInfo : LibResources.PostPluralTargetInfo) +
                           ")";

                return Target.ToLongDateString() + time;
            }
        }

        public String PinnedString
        {
            get { return _countdown.Pinned ? LibResources.Yes : LibResources.No; }
        }

        public int DaysToCome
        {
            get { return (int) (Target - DateTime.Now).TotalDays; }
        }

        public Countdown Model
        {
            get { return _countdown; }
        }

        #endregion

        #region Constructors

        public CountDownViewModel(CountDownViewModel parent)
        {
            _countdown = new Countdown(parent._countdown);
        }

        public CountDownViewModel(Countdown parent)
        {
            _countdown = parent;
        }

        #endregion

        #region IComparable<CountDownViewModel>

        public int CompareTo(CountDownViewModel other)
        {
            if (Target < other.Target)
                return -1;
            if (Target > other.Target)
                return +1;
            return 0;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        //TODO:[NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public void UpdateTimerBindings()
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs("DaysToComeMsg"));
                handler(this, new PropertyChangedEventArgs("PreciseSpanMsg"));
            }
        }

        #endregion
    }
}