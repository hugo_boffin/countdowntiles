﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using CountdownLib.Models;

namespace CountdownLib.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        // TODO: Müll, CountdownRepository nutzen -> evtl. im Beispiel nachlesen
        private readonly ObservableCollection<CountDownViewModel> _countdowns;
        private readonly CountdownRepository repository;

        public MainViewModel(CountdownRepository repository)
        {
            this.repository = repository;
            _countdowns = new ObservableCollection<CountDownViewModel>();

            foreach (Countdown countdown in repository.Countdowns)
            {
                _countdowns.Add(new CountDownViewModel(countdown));
            }
        }

        public MainViewModel()
        {
            repository = new CountdownRepository();
            _countdowns = new ObservableCollection<CountDownViewModel>();
        }

        public CountdownRepository Repository
        {
            get { return repository; }
        }
        
        public ObservableCollection<CountDownViewModel> CountDownViewModels
        {
            get { return _countdowns; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public void RemoveCountdown(CountDownViewModel cdvm)
        {
            _countdowns.Remove(cdvm);
            Repository.Countdowns.Remove(cdvm.Model);
        }

        public void AddCountdown(CountDownViewModel cdvm)
        {
            _countdowns.Add(cdvm);
            Repository.Countdowns.Add(cdvm.Model);
        }
    }
}